import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { NgbCarouselConfig, NgbCarousel } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
const particlesJson = require('particles.json');

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [NgbCarouselConfig]
})
export class HomeComponent implements OnInit {
  @ViewChild('myCarousel') myCarousel: NgbCarousel;
  currentSlideIndex = '0';
  indicators: NodeListOf<Element>;
  indicator_bar: NodeListOf<Element>;
  sliderIndicatorBarWidth = 0;
  sliderInterval = 5000;
  // _sliderIndicatorBar: NodeJS.Timer;
  _sliderIndicatorBar: any;
  barWidth = 0;

  pjs_style2: object = {};
  pjs_params2: object = {};
  pjs_width2 = 100;
  pjs_height2 = 100;

  slides: any;

  @Input() page_data: any;

  constructor(
    config: NgbCarouselConfig,
    private route: ActivatedRoute) {
    config.interval = this.sliderInterval;
    config.keyboard = false;
    config.pauseOnHover = false;
    config.showNavigationArrows = false;
    config.showNavigationIndicators = false;
  }

  ngOnInit() {
    console.log(this.page_data, 'home_data');
    this.slides = this.page_data.data;

    this.indicators = document.querySelectorAll('.slider-nav > ul > li');
    this.indicator_bar = document.querySelectorAll('.slider-nav > ul > li > span');
    this.setIndicator();

    this.pjs_style2 = {
      position: 'absolute',
      width: '100vw',
      height: '100vh',
      'z-index': 1
    };

    this.pjs_params2 = particlesJson;
  }

  get_data(data) {
    // console.log(data.data, 'data');
  }

  onslideChange(event) {
    this.currentSlideIndex = event.current;
    this.setIndicator();
  }

  setIndicator() {
    // console.log(this.currentSlideIndex, 'currentSlideIndex');
    [].forEach.call(this.indicators, (element) => {
      element.classList.remove('active');
    });

    this.barWidth = 0;
    clearInterval(this._sliderIndicatorBar);
    this.indicators[this.currentSlideIndex].className += 'active';

    this._sliderIndicatorBar = setInterval(() => {
      this.barWidth = this.barWidth + 0.6;
      this.indicator_bar[this.currentSlideIndex].style.width = this.barWidth + '%';
      if (this.barWidth >= 100) {
        clearInterval(this._sliderIndicatorBar);
      }
    }, 30);
  }


}
