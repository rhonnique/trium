import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CompanyComponent } from './company/company.component';
import { ConsultingComponent } from './consulting/consulting.component';
import { CapitalComponent } from './capital/capital.component';
import { ContactComponent } from './contact/contact.component';
import { ThinkingComponent } from './thinking/thinking.component';
import { TeamComponent } from './team/team.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { ApplyComponent } from './apply/apply.component';
import { CareersComponent } from './careers/careers.component';

const routes: Routes = [
  { path: '', component: HomeComponent, data: { index: 0, inverse: 0, inner_page: false } },
  { path: 'company', component: CompanyComponent, data: { index: 1, inverse: 1, inner_page: false } },
  { path: 'capital', component: CapitalComponent, data: { index: 2, inverse: 1, inner_page: false, page: 'capital' } },
  { path: 'consulting', component: ConsultingComponent, data: { index: 3, inverse: 1, inner_page: false, page: 'consulting' } },
  { path: 'careers', component: CareersComponent, data: { index: 4, inverse: 0, inner_page: false, page: 'careers' } },
  //{ path: 'portfolio', component: PortfolioComponent, data: { index: 4, inverse: 0, inner_page: false, page: 'portfolio' } },
  { path: 'apply', component: ApplyComponent, data: { index: 5, inverse: 0, inner_page: false, page: 'apply' } },
  /* { path: 'team', component: TeamComponent, data: { index: 5, inverse: 0, inner_page: false, page: 'team' } }, */
  { path: 'thinking', component: ThinkingComponent, data: { index: 6, inverse: 1, inner_page: false, page: 'thinking' } },
  { path: 'contact', component: ContactComponent, data: { index: 7, inverse: 1, inner_page: false, page: 'contact' } },
  //{ path: 'careers', component: CareersComponent, data: { inverse: 0, inner_page: true, page: 'careers' } },
  /* { path: 'apply', component: ApplyComponent, data: { inverse: 0, inner_page: true, page: 'apply' } }, */
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
