import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { map, throttle, catchError } from 'rxjs/operators';
import { of, Observable, throwError } from 'rxjs';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/map';
import * as $ from 'jquery';

@Injectable({
  providedIn: 'root'
})
export class ModelsService {
  site_url = 'https://www.trium.ng/admin';
  // site_url = 'http://localhost/trium';
  api_link = this.site_url + '/api';
  timeOut = 90000;
  google_site_key = '6LddwIQUAAAAAPKAeQgX5pP-ox5yfwmXNIKJlF4X';
  private app_data = null;
  shareData = {};

  constructor(private httpClient: HttpClient) { }

  public getAppData() {
    return this.app_data;
  }

  load() {
    return new Promise((resolve, reject) => {
      this.httpClient
        .get(this.api_link)
        .map(res => res)
        .subscribe(response => {
          this.app_data = response['pages'];
          resolve(true);
        });
    });
  }

  contact(value: any) {
    return this.httpClient.post(this.api_link + '/contact', value)
      .timeout(this.timeOut)
      .pipe(
        catchError(this.handleError)
      );
  }

  apply(value: any) {
    return this.httpClient.post(this.api_link + '/apply', value)
      .timeout(this.timeOut)
      .pipe(
        catchError(this.handleError)
      );
  }

  careers(value: any) {
    return this.httpClient.post(this.api_link + '/careers', value)
      .timeout(this.timeOut)
      .pipe(
        catchError(this.handleError)
      );
  }

  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    // return Observable.of('data not available at this time');
    return throwError({
      status: error.status,
      message: error.message,
      name: error.name,
    });
  }

  validate_captcha(captchaResponse: string) {
    console.log(captchaResponse, 'captchaResponse');
    if (captchaResponse !== null) {
      return true;
    }
    return false;
  }

  scrollEvent(viewClass, banner): void {
    const doc = document.getElementsByClassName(viewClass)[0];
    const top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);

    if (banner !== null) {
      $('.is-parallax').css('background-position', 'center calc(50% + -' + (top / 2) + 'px)');
    }

    if (top >= 50) {
      $('header').addClass('is-scrolled');
    } else {
      $('header').removeClass('is-scrolled');
    }
  }

}
