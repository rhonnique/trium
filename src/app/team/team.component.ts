import { Component, OnInit, Input } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';
import * as $ from 'jquery';
import { ModelsService } from '../models.service';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {
  browser: any;
  viewClass = 'scroll-team';
  @Input() page_data: any;
  teams: any;
  team_details: any;
  site_url: any;

  constructor(
    private deviceService: DeviceDetectorService,
    private modelsService: ModelsService
  ) {
    this.site_url = modelsService.site_url;
   }

  ngOnInit() {
    this.browser = this.deviceService.getDeviceInfo().browser;
    this.teams = this.page_data.data;
  }

  public onScrollEvent(event: any): void {
    const doc = document.getElementsByClassName(this.viewClass)[0];
    const top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
    if (top >= 50) {
      $('header').addClass('is-scrolled');
    } else {
      $('header').removeClass('is-scrolled');
    }
    $('.is-parallax').css('background-position', 'center calc(50% + -' + (top / 2) + 'px)');
  }

  show_modal(data) {
    this.team_details = data;
    $('.team-modal-wrapper').fadeIn('linear');
  }

  hide_modal() {
    this.team_details = {};
    $('.team-modal-wrapper').fadeOut('linear');
  }

}
