import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router, RoutesRecognized } from '@angular/router';
import { DeviceDetectorService } from 'ngx-device-detector';
import * as $ from 'jquery';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {
  browser: any;
  viewClass = 'scroll-portfolio';
  @Input() page_data: any;
  portfolios: any;
  details: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private deviceService: DeviceDetectorService
  ) { }

  ngOnInit() {
    this.browser = this.deviceService.getDeviceInfo().browser;
    this.portfolios = this.page_data.data;
  }

  public onScrollEvent(event: any): void {
    const doc = document.getElementsByClassName(this.viewClass)[0];
    const top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
    if (top >= 50) {
      $('header').addClass('is-scrolled');
    } else {
      $('header').removeClass('is-scrolled');
    }
    $('.is-parallax').css('background-position', 'center calc(50% + -' + (top / 2) + 'px)');
  }

  show_modal(data) {
    this.details = data;
    $('.portfolio-modal-wrapper').fadeIn('linear');
  }

  hide_modal() {
    $('.portfolio-modal-wrapper').fadeOut('linear');
  }
}
