import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { HttpClientModule } from '@angular/common/http';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RecaptchaModule, RECAPTCHA_SETTINGS, RecaptchaSettings } from 'ng-recaptcha';


import { ScrollbarModule } from 'ngx-scrollbar';
import { DeviceDetectorModule } from 'ngx-device-detector';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { SlickModule } from 'ngx-slick';
import { ParticlesModule } from 'angular-particle';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { CompanyComponent } from './company/company.component';
import { ConsultingComponent } from './consulting/consulting.component';
import { ThinkingComponent } from './thinking/thinking.component';
import { TeamComponent } from './team/team.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { CapitalComponent } from './capital/capital.component';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { FooterComponent } from './footer/footer.component';
import { ApplyComponent } from './apply/apply.component';
import { CareersComponent } from './careers/careers.component';
import { ModelsService } from './models.service';
import { Module1Component } from './module1/module1.component';
import { Module2Component } from './module2/module2.component';
import { Module3Component } from './module3/module3.component';
import { Module4Component } from './module4/module4.component';
import { Module5Component } from './module5/module5.component';
import { Module6Component } from './module6/module6.component';
import { Module7Component } from './module7/module7.component';
import { Module8Component } from './module8/module8.component';
import { BannerComponent } from './banner/banner.component';

export function modelsServiceFactory(provider: ModelsService) {
  return () => provider.load();
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContactComponent,
    CompanyComponent,
    ConsultingComponent,
    ThinkingComponent,
    TeamComponent,
    PortfolioComponent,
    CapitalComponent,
    FooterComponent,
    ApplyComponent,
    CareersComponent,
    Module1Component,
    Module2Component,
    Module3Component,
    Module4Component,
    Module5Component,
    Module6Component,
    Module7Component,
    Module8Component,
    BannerComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      progressBar: true,
      extendedTimeOut: 3000,
      preventDuplicates: true
    }),
    AppRoutingModule,
    ScrollbarModule,
    PerfectScrollbarModule,
    DeviceDetectorModule.forRoot(),
    SlickModule.forRoot(),
    NgbModule,
    CarouselModule.forRoot(),
    ParticlesModule,
    HttpClientModule,

    FormsModule,
    ReactiveFormsModule,
    RecaptchaModule
  ],
  providers: [
    ModelsService,
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    {
      provide: APP_INITIALIZER,
      useFactory: modelsServiceFactory,
      deps: [ModelsService], multi: true
    },
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: {
        siteKey: '6LddwIQUAAAAAPKAeQgX5pP-ox5yfwmXNIKJlF4X'
      } as RecaptchaSettings,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
