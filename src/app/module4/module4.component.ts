import { Component, OnInit, Input } from '@angular/core';
import { ModelsService } from '../models.service';

@Component({
  selector: 'app-module4',
  templateUrl: './module4.component.html',
  styleUrls: ['./module4.component.css']
})
export class Module4Component implements OnInit {
  site_url: any;
  @Input() module_data: any;

  constructor(
    private modelsService: ModelsService
  ) {
    this.site_url = modelsService.site_url;
  }

  ngOnInit() {
  }

}
