import {
  Component,
  OnInit,
  ElementRef,
  AfterViewInit,
  ComponentFactoryResolver,
  ViewContainerRef,
  ViewChild
} from '@angular/core';
import { ActivatedRoute, Router, RoutesRecognized } from '@angular/router';
import { CapitalComponent } from './capital/capital.component';
import { HomeComponent } from './home/home.component';
import { CompanyComponent } from './company/company.component';
import { ConsultingComponent } from './consulting/consulting.component';
import * as $ from 'jquery';
const particlesJson = require('particles.json');
import { routingAnimation } from './app-routing-animations';
import { ModelsService } from './models.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [routingAnimation]
})
export class AppComponent implements OnInit, AfterViewInit {
  @ViewChild('is_home', { read: ViewContainerRef }) is_home: ViewContainerRef;
  @ViewChild('is_company', { read: ViewContainerRef }) is_company: ViewContainerRef;
  @ViewChild('is_capital', { read: ViewContainerRef }) is_capital: ViewContainerRef;
  @ViewChild('is_consulting', { read: ViewContainerRef }) is_consulting: ViewContainerRef;

  title = 'trium';
  pages_state = 'up';
  show_menu = false;
  width: number;
  current_index: number;
  td_page: NodeListOf<Element>;
  tr_page: Element;
  tr_page_style = {};
  td_page_style = {};
  body: HTMLBodyElement;
  tr_page_width: number;
  page_offset: number;
  header: HTMLElement;
  inverse: number;
  td_page_style2: { width: string };
  slide_lock = false;
  inner_page: boolean;

  pjs_style: object = {};
  pjs_params: object = {};
  pjs_width = 100;
  pjs_height = 100;
  slide_index: number;

  app_data: any;

  constructor(
    private elRef: ElementRef,
    private router: Router,
    private _cfr: ComponentFactoryResolver,
    private modelsService: ModelsService
  ) {
  }

  ngOnInit() {
    this.app_data = this.modelsService.getAppData();
    this.modelsService.shareData = this.app_data;

    this.pjs_style = {
      position: 'absolute',
      width: '100vw',
      height: '100vh',
      'z-index': 0
    };

    this.pjs_params = particlesJson;

    // this.addComponent();
    this.router.events.subscribe(data => {
      if (data instanceof RoutesRecognized) {
        /* Subscript current page index from route */
        this.current_index = data.state.root.firstChild.data.index;
        this.inverse = data.state.root.firstChild.data.inverse;
        this.inner_page = data.state.root.firstChild.data.inner_page;

        /* Get all pages elements */
        this.td_page = document.querySelectorAll('.td-page');
        /* Get body element */
        this.body = document.getElementsByTagName('body')[0];
        /* Get header element */
        this.header = document.getElementsByTagName('header')[0];

        if (this.inner_page === true) {
          console.log('inner_page');
        } else {

          if (this.inverse === 1) {
            // this.header.className += " inverse";
            $('header').addClass('inverse');
          } else {
            // this.header.classList.remove("inverse");
            $('header').removeClass('inverse');
          }

          /* Get current window width */
          this.width = window.innerWidth;

          [].forEach.call(this.td_page, function (element) {
            element.classList.remove('is-active');
          });

          console.log(this.current_index, 'this.current_index');

          this.td_page[this.current_index].className += ' is-active';

          /* Get all pages wrapper element */
          this.tr_page = document.getElementsByClassName('t-pages')[0];

          /* Set active menu */
          const menus = document.querySelectorAll('nav.main a');
          // forEach(menus, function (index, value) {
          [].forEach.call(menus, function (element) {
            element.setAttribute('class', '');
          });
          menus[this.current_index].className = 'active';
          this.page_offset = this.current_index * 100;
        }

        /* Get total number of pages and sum it */
        const count_all_pages = this.td_page.length;
        this.tr_page_width = count_all_pages * this.width;

        /* Set each page width */
        // this.tr_page_style = Object.assign(this.tr_page_style, { 'width': this.tr_page_width + 'px' });
        /* this.td_page_style = Object.assign(this.td_page_style, { 'width': this.width + 'px' }); */

        /* On window resize, reset pages and container widths */
        window.addEventListener('resize', () => {
          /* get new widths and assign to width variables */
          const new_width = window.innerWidth;
          this.width = new_width;
          this.tr_page_width = count_all_pages * this.width;

          if (this.body.classList.contains('pages-down-command-R')) {
            this.tr_page_style = Object.assign(this.tr_page_style, {
              width: this.tr_page_width + 'px'
            });
            this.td_page_style = Object.assign(this.td_page_style, {
              width: this.width + 'px'
            });
          }
          // this.tr_page_style = Object.assign(this.tr_page_style, { 'width': this.tr_page_width + 'px' });
        });
      }
    });
    /* Subscribe ends */
  }

  ngAfterViewInit() {
    // $('ng-scrollbar').remove();
    /* $('nav.main a').hover(
      () => {
        $('nav.main a.active')
          .addClass('inactive')
          .removeClass('active');
      },
      () => {
        $('nav.main a.inactive')
          .addClass('active')
          .removeClass('inactive');
      }
    ); */
  }

  toggle_page() {
    this.show_menu = this.show_menu === false ? true : false;
    switch (this.show_menu) {
      case true:
        // $('header').removeClass('header-fix-white');

        /* Set each window width */
        this.td_page_style = Object.assign(this.td_page_style, {
          width: this.width + 'px'
        });
        /* Add perspective properties to body */
        this.body.setAttribute(
          'style',
          '-webkit-perspective: 1600px; perspective: 1600px; position: fixed;'
        );
        // this.tr_page.className += ' animate-transform';
        this.body.className = 'pages-down-command-R';
        this.tr_page_style = Object.assign(
          {},
          {
            left: '-' + this.page_offset + '%',
            // 'transform-origin': '50% 50% 0px',
            width: this.tr_page_width + 'px',
            transform: 'translate3d(0, 55%, -1000px)',
            transition: 'transform 1s',
            '-webkit-transition': 'transform 1s'
          }
        );

        setTimeout(() => {
          // this.tr_page.classList.remove('animate-transform');
        }, 1000);
        break;
      case false:
        if (
          this.current_index !== this.slide_index &&
          this.slide_index !== undefined
        ) {
          this.hover(this.current_index);
          setTimeout(() => {
            this.toggle_page_false();
            this.slide_index = undefined;
          }, 400);
        } else {
          this.toggle_page_false();
          this.slide_index = undefined;
        }
        break;
      default:
    }
  }

  toggle_page_false() {
    this.tr_page_style = Object.assign(this.tr_page_style, {
      transform: 'translate3d(0, 0px, 0)'
    });

    setTimeout(() => {
      this.body.setAttribute('style', '');
      this.body.classList.remove('pages-down-command-R');
      // this.tr_page.classList.remove('animate-transform');
      this.tr_page_style = Object.assign({});
      this.td_page_style = Object.assign({});
    }, 1000);
  }

  hover(index: number) {
    if (index !== this.slide_index) {
      const pages_wrapper = $('.t-pages');
      this.slide_index = index;

      $('nav.main a').removeClass('active');
      $('nav.main a:eq(' + index + ')').addClass('active');

      pages_wrapper
        .stop(true)
        .find('.td-page')
        .removeClass('is-active')
        .promise()
        .done(() => {
          pages_wrapper
            .animate({ left: '-' + index * this.width + 'px' }, 1200, 'linear')
            .promise()
            .done(() => {
              pages_wrapper
                .find('.td-page:eq(' + index + ')')
                .addClass('is-active');
            });
        });
    }
  }

  goto(index: number, route, inverse: number = 0) {
    if (index === this.current_index) {
      this.frame_set_active_page(index);
      setTimeout(() => {
        /* Reset and clean up page */
        this.frame_reset_page();
      }, 1000);
    } else {
      if (this.body.classList.contains('pages-down-command-R')) {
        // console.log(this.current_index, "show:goto index: " + index);
        this.page_offset = index * 100;
        /* this.td_page[this.current_index].classList.remove('is-active'); */
        [].forEach.call(this.td_page, element => {
          element.classList.remove('is-active');
        });

        setTimeout(() => {
          /* Slide to selected page */
          this.frame_slide_to(this.page_offset);
          setTimeout(() => {
            /* Activate page */
            this.frame_set_active_page(index);
            setTimeout(() => {
              /* Reset and clean up page */
              this.frame_reset_page();
            }, 1000);
            setTimeout(() => {
              /* Navigate to selected page */
              this.router.navigate([route]);
            }, 1000);
          }, 1000);
        }, 500);
      } else {
        this.router.navigate([route]);
      }
    }
  }

  frame_slide_to(offset) {
    this.tr_page_style = Object.assign(this.tr_page_style, {
      left: '-' + offset + '%',
      transition: 'left 1s',
      '-webkit-transition': 'left 1s'
    });
  }

  frame_set_active_page(index: number) {
    const new_page = this.td_page[index];
    new_page.className += ' is-active';
    // this.tr_page.classList.remove('transform-left');

    this.tr_page_style = Object.assign(this.tr_page_style, {
      transform: 'translate3d(0, 0px, 0)',
      transition: 'transform 1s',
      '-webkit-transition': 'transform 1s'
    });
  }

  frame_reset_page() {
    this.tr_page_style = Object.assign(
      {},
      {
        transition: 'transform 0s',
        width: this.width + 'px'
      }
    );
    this.body.setAttribute('style', '');
    this.body.classList.remove('pages-down-command-R');
    this.show_menu = false;
    this.tr_page.setAttribute('style', '');
    setTimeout(() => {
      this.tr_page_style = Object.assign({});
      this.td_page_style = Object.assign({});
    }, 0);
  }

  addComponent() {
    const homeComponent = this._cfr.resolveComponentFactory(HomeComponent);
    this.is_home.createComponent(homeComponent);

    const companyComponent = this._cfr.resolveComponentFactory(
      CompanyComponent
    );
    this.is_company.createComponent(companyComponent);

    const capitalComponent = this._cfr.resolveComponentFactory(
      CapitalComponent
    );
    this.is_capital.createComponent(capitalComponent);

    const consultingComponent = this._cfr.resolveComponentFactory(
      ConsultingComponent
    );
    this.is_consulting.createComponent(consultingComponent);
  }
}
