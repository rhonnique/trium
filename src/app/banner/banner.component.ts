import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ModelsService } from '../models.service';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {
  site_url: any;
  @Input() banner: any;
  @Output() scrollEvent = new EventEmitter();

  constructor(private modelsService: ModelsService) {
    this.site_url = modelsService.site_url;
  }

  ngOnInit() { }

  emitScroll() {
    this.scrollEvent.emit();
  }

}
