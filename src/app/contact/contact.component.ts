import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';
import * as $ from 'jquery';
import { ModelsService } from '../models.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  browser: any;
  is_parallax: NodeListOf<Element>;
  viewClass = 'scroll-contact';
  @Input() page_data: any;
  partners: any;
  site_url: any;

  contact_form: FormGroup;
  site_key: string;
  contact_form_valid = false;
  @ViewChild('captchaRef') captchaRef: ElementRef;
  submitted = false;

  constructor(
    private deviceService: DeviceDetectorService,
    private modelsService: ModelsService,
    private fb: FormBuilder,
    private elementRef: ElementRef,
    private toastr: ToastrService
  ) {
    this.site_url = modelsService.site_url;
    this.site_key = modelsService.google_site_key;
  }

  ngOnInit() {
    this.browser = this.deviceService.getDeviceInfo().browser;
    this.is_parallax = document.querySelectorAll('.is-parallax');

    this.partners = this.page_data.partners;

    this.contact_form = this.fb.group({
      name: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]],
      phone: [null, [Validators.minLength(9)/* ,
      Validators.pattern('[0-9]{0-10}') */]],
      subject: [null, Validators.required],
      message: [null, Validators.required],
      // recaptchaReactive: [null, Validators.required],
    });

    console.log(this.contact_form_valid, 'val');
  }

  get f() { return this.contact_form.controls; }

  public onScrollEvent(event: any): void {
    const banner = this.page_data.banner.image;
    this.modelsService.scrollEvent(this.viewClass, banner);
  }

  contactSubmit(value: any) {
    this.submitted = true;
    if (this.contact_form.invalid) {
      return false;
    }
    $('[name=contact_form] button').html('sending, please wait..');
    $('[name=contact_form] input, [name=contact_form] textarea,  [name=contact_form] button').prop('disabled', true);
    this.modelsService.contact(value).subscribe(res => {
      this.ResetForm(true);
      this.toastr.success('Your message has been sent successfully. We will get back to you shortly!', 'Message Sent!');
      this.captchaRef.reset();
    }, err => {
      this.toastr.error('Your message was not sent. Please retry!', 'Message Not Sent!');
      this.ResetForm();
    });
  }

  ResetForm(reset: Boolean = false): void {
    $('[name=contact_form] button').html('Send Message');
    $('[name=contact_form] input, [name=contact_form] textarea,  [name=contact_form] button').prop('disabled', false);
    if (reset === true) {
      this.contact_form.reset();
    }
  }

  validate_captcha(captchaResponse: string) {
    if (captchaResponse !== null) {
      this.contact_form_valid = true;
    } else {
      this.contact_form_valid = false;
    }
  }

}
