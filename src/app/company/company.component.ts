import { Component, OnInit, AfterViewInit, ViewChild, Input, ElementRef } from '@angular/core';
import * as $ from 'jquery';
import { DeviceDetectorService } from 'ngx-device-detector';
import { NgbCarouselConfig, NgbCarousel } from '@ng-bootstrap/ng-bootstrap';
import { ModelsService } from '../models.service';
@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css'],
  providers: [NgbCarouselConfig]
})
export class CompanyComponent implements OnInit, AfterViewInit {
  @ViewChild('myCarousel') myCarousel: NgbCarousel;
  browser: any;
  is_parallax: NodeListOf<Element>;
  scroll_page = true;
  viewClass = 'scroll-company';
  sliderInterval = 5000;
  @Input() page_data: any;
  modules: any;

  constructor(
    private deviceService: DeviceDetectorService,
    carouselConfig: NgbCarouselConfig,
    private modelsService: ModelsService
  ) {
    carouselConfig.interval = this.sliderInterval;
    carouselConfig.keyboard = false;
    carouselConfig.pauseOnHover = false;
    carouselConfig.showNavigationArrows = false;
    carouselConfig.showNavigationIndicators = true;
   }

  ngOnInit() {
    this.browser = this.deviceService.getDeviceInfo().browser;
    this.is_parallax = document.querySelectorAll('.is-parallax');
    this.modules = this.page_data.data;
  }

  ngAfterViewInit() {
  }

  public onScrollEvent(event: any): void {
    const banner = this.page_data.banner.image;
    this.modelsService.scrollEvent(this.viewClass, banner);
  }

}
