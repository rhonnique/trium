import { Component, OnInit, Input } from '@angular/core';
import { ModelsService } from '../models.service';

@Component({
  selector: 'app-module5',
  templateUrl: './module5.component.html',
  styleUrls: ['./module5.component.css']
})
export class Module5Component implements OnInit {
  site_url: any;
  @Input() module_data: any;

  constructor(
    private modelsService: ModelsService
  ) {
    this.site_url = modelsService.site_url;
  }

  ngOnInit() {
  }

}
