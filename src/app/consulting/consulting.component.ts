import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';
import * as $ from 'jquery';
import { ModelsService } from '../models.service';

@Component({
  selector: 'app-consulting',
  templateUrl: './consulting.component.html',
  styleUrls: ['./consulting.component.css']
})
export class ConsultingComponent implements OnInit {
  browser: any;
  is_parallax: NodeListOf<Element>;
  viewClass = 'scroll-consulting';
  @Input() page_data: any;
  modules: any;

  constructor(
    private deviceService: DeviceDetectorService,
    private modelsService: ModelsService,
  ) { }

  ngOnInit() {
    this.browser = this.deviceService.getDeviceInfo().browser;
    this.is_parallax = document.querySelectorAll('.is-parallax');
    this.modules = this.page_data.data;
  }

  public onScrollEvent(event: any): void {
    const banner = this.page_data.banner.image;
    this.modelsService.scrollEvent(this.viewClass, banner);
  }

}
