import { Component, OnInit, Input } from '@angular/core';
import { ModelsService } from '../models.service';

@Component({
  selector: 'app-module3',
  templateUrl: './module3.component.html',
  styleUrls: ['./module3.component.css']
})
export class Module3Component implements OnInit {
  site_url: any;
  @Input() module_data: any;

  constructor(
    private modelsService: ModelsService
  ) {
    this.site_url = modelsService.site_url;
  }

  ngOnInit() {
  }

}
