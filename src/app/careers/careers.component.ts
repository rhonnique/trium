import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import * as $ from 'jquery';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DeviceDetectorService } from 'ngx-device-detector';
import { ModelsService } from '../models.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-careers',
  templateUrl: './careers.component.html',
  styleUrls: ['./careers.component.css']
})
export class CareersComponent implements OnInit {
  viewClass = 'scroll-careers';
  careers_form: FormGroup;
  captcha_valid = false;
  upload_valid = false;
  uploaded_file: File;
  @ViewChild('captchaRef') captchaRef: ElementRef;
  site_url: string;
  submitted = false;
  // page_data: any;
  @Input() page_data: any;

  constructor(
    private deviceService: DeviceDetectorService,
    private modelsService: ModelsService,
    private fb: FormBuilder,
    private elementRef: ElementRef,
    private toastr: ToastrService
  ) {
    this.site_url = modelsService.site_url;
  }

  ngOnInit() {
    this.careers_form = this.fb.group({
      name: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]],
      file_upload: [null, Validators.required],
      phone: [null, [Validators.minLength(9)]],
      message: [null]
    });

    // this.page_data = this.modelsService.shareData['careers'];

  }

  get f() { return this.careers_form.controls; }

  public onScrollEvent(event: any): void {
    const banner = this.page_data.banner.image;
    this.modelsService.scrollEvent(this.viewClass, banner);
  }

  careersSubmit(value: any) {
    this.submitted = true;
    if (this.careers_form.invalid) {
      return false;
    }
    const formData: any = new FormData();
    formData.append('name', value['name']);
    formData.append('email', value['email']);
    formData.append('phone', value['phone']);
    formData.append('message', value['message']);
    formData.append('file', this.uploaded_file);
    $('[name=careers_form] button').html('sending, please wait..');
    $('[name=careers_form] input, [name=careers_form] textarea,  [name=careers_form] button').prop('disabled', true);
    this.modelsService.careers(formData).subscribe(() => {
      this.ResetForm(true);
      this.toastr.success('Your résumé has been submitted successfully. We will get back to you shortly!', 'Résumé Submitted!');
      this.captchaRef.reset();
    }, () => {
      this.toastr.error('Your Résumé was not submitted. Please retry!', 'Résumé Not Submitted!');
      this.ResetForm();
    });
  }

  ResetForm(reset: Boolean = false): void {
    $('[name=careers_form] button').html('Submit');
    $('[name=careers_form] input, [name=careers_form] textarea,  [name=careers_form] button').prop('disabled', false);
    if (reset === true) {
      this.submitted = false;
      this.careers_form.reset();
      $('#file_upload').replaceWith($('#file_upload+').val('').clone(true));
    }
  }

  validate_captcha(captchaResponse: string) {
    if (captchaResponse !== null) {
      this.captcha_valid = true;
    } else {
      this.captcha_valid = false;
    }
  }

  validate_upload(files) {
    const uploaded_file = files[0];
    if (uploaded_file !== undefined) {
      this.upload_valid = true;
      this.uploaded_file = uploaded_file;
      $('.upload-placeholder').html(uploaded_file.name);
    }
  }

}
