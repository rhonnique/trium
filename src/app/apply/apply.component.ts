import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import * as $ from 'jquery';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DeviceDetectorService } from 'ngx-device-detector';
import { ModelsService } from '../models.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-apply',
  templateUrl: './apply.component.html',
  styleUrls: ['./apply.component.css']
})
export class ApplyComponent implements OnInit {

  viewClass = 'scroll-apply';
  apply_form: FormGroup;
  captcha_valid = false;
  upload_valid = false;
  uploaded_file: File;
  @ViewChild('captchaRef') captchaRef: ElementRef;
  site_url: string;
  @Input() page_data: any;
  submitted = false;

  constructor(
    private deviceService: DeviceDetectorService,
    private modelsService: ModelsService,
    private fb: FormBuilder,
    private elementRef: ElementRef,
    private toastr: ToastrService
  ) {
    this.site_url = modelsService.site_url;
  }

  ngOnInit() {

    this.apply_form = this.fb.group({
      name: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]],
      phone: [null, [Validators.minLength(9)]],
      message: [null],
      file_upload: [null, Validators.required],
    });

  }

  get f() { return this.apply_form.controls; }

  public onScrollEvent(event: any): void {
    const banner = this.page_data.banner.image;
    this.modelsService.scrollEvent(this.viewClass, banner);
  }

  applySubmit(value: any) {
    this.submitted = true;
    if (this.apply_form.invalid) {
      return false;
    }
    const formData: any = new FormData();
    formData.append('name', value['name']);
    formData.append('email', value['email']);
    formData.append('phone', value['phone']);
    formData.append('message', value['message']);
    formData.append('file', this.uploaded_file);
    $('[name=apply_form] button').html('sending, please wait..');
    $('[name=apply_form] input, [name=apply_form] textarea,  [name=apply_form] button').prop('disabled', true);
    this.modelsService.apply(formData).subscribe(res => {
      this.ResetForm(true);
      this.toastr.success('Your application has been submitted successfully. We will get back to you shortly!', 'Application Submitted!');
      this.captchaRef.reset();
    }, err => {
      this.toastr.error('Your application was not submitted. Please retry!', 'Application Not Submitted!');
      this.ResetForm();
    });
  }

  ResetForm(reset: Boolean = false): void {
    $('[name=apply_form] button').html('Apply');
    $('[name=apply_form] input, [name=apply_form] textarea,  [name=apply_form] button').prop('disabled', false);
    if (reset === true) {
      this.submitted = false;
      this.apply_form.reset();
    }
  }

  validate_captcha(captchaResponse: string) {
    if (captchaResponse !== null) {
      this.captcha_valid = true;
    } else {
      this.captcha_valid = false;
    }
  }

  validate_upload(files) {
    const uploaded_file = files[0];
    if (uploaded_file !== undefined) {
      this.upload_valid = true;
      this.uploaded_file = uploaded_file;
      $('.upload-placeholder').html(uploaded_file.name);
    }
  }

}
