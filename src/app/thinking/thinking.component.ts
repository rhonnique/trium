import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';
import * as $ from 'jquery';
import { ModelsService } from '../models.service';

@Component({
  selector: 'app-thinking',
  templateUrl: './thinking.component.html',
  styleUrls: ['./thinking.component.css']
})
export class ThinkingComponent implements OnInit {
  browser: any;
  is_parallax: NodeListOf<Element>;
  viewClass = 'scroll-thinking';
  @Input() page_data: any;
  posts: any;
  site_url: any;

  constructor(
    private deviceService: DeviceDetectorService,
    private modelsService: ModelsService
  ) {
    this.site_url = modelsService.site_url;
  }

  ngOnInit() {
    this.browser = this.deviceService.getDeviceInfo().browser;
    this.is_parallax = document.querySelectorAll('.is-parallax');
    this.posts = this.page_data.data;
  }

  public onScrollEvent(event: any): void {
    const banner = this.page_data.banner.image;
    this.modelsService.scrollEvent(this.viewClass, banner);
  }

  objectKey(obj) {
    return Object.keys(obj);
  }

  filterPost() {
    return this.posts.reduce((prev, now) => {
      if (!prev[now.category_name]) {
        prev[now.category_name] = [];
      }

      prev[now.category_name].push(now);
      return prev;
    }, {});
  }

  open_link(link) {
    window.open(link, '_blank');
  }

}
