import { Component, OnInit, AfterViewInit, Input, ViewChild, ElementRef } from '@angular/core';
import * as $ from 'jquery';
import { DeviceDetectorService } from 'ngx-device-detector';
import { ModelsService } from '../models.service';

@Component({
  selector: 'app-capital',
  templateUrl: './capital.component.html',
  styleUrls: ['./capital.component.css']
})
export class CapitalComponent implements OnInit, AfterViewInit {
  _ref: any;
  browser: any;
  doc: Element;
  is_parallax: any;
  viewClass = 'scroll-capital';
  @Input() page_data: any;
  modules: any;

  constructor(
    private deviceService: DeviceDetectorService,
    private modelsService: ModelsService
  ) { }

  ngOnInit() {
    $('header').removeClass('header-fix-white');
    this.browser = this.deviceService.getDeviceInfo().browser;
    this.modules = this.page_data.data;
  }

  ngAfterViewInit() {}

  public onScrollEvent(event: any): void {
    const banner = this.page_data.banner.image;
    this.modelsService.scrollEvent(this.viewClass, banner);
  }

}
