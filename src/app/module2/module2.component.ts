import { Component, OnInit, Input } from '@angular/core';
import { ModelsService } from '../models.service';

@Component({
  selector: 'app-module2',
  templateUrl: './module2.component.html',
  styleUrls: ['./module2.component.css']
})
export class Module2Component implements OnInit {
  site_url: any;
  @Input() module_data: any;

  constructor(
    private modelsService: ModelsService
  ) {
    this.site_url = modelsService.site_url;
  }

  ngOnInit() {
  }

}
