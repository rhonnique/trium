import { Component, OnInit, Input } from '@angular/core';
import { ModelsService } from '../models.service';

@Component({
  selector: 'app-module1',
  templateUrl: './module1.component.html',
  styleUrls: ['./module1.component.css']
})
export class Module1Component implements OnInit {
  site_url: any;
  @Input() module_data: any;

  constructor(
    private modelsService: ModelsService
  ) {
    this.site_url = modelsService.site_url;
  }

  ngOnInit() {
  }

}
